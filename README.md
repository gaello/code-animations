# Coding Animations in Unity

So you are looking for information how to code your own animations in Unity? That's easy! This repository contain what you looking for! There is one base animation script from each animation inherit some basic logic.

In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2019/04/22/coding-animations-in-unity/

Enjoy!

---

# How to use it?

This repository contains an example of how you can code own animations in Unity.

If you want to see that implementation, go straight to [Assets/Scripts/](https://bitbucket.org/gaello/code-animations/src/master/Assets/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

#Well done!

You have just learned how to code own animations in Unity!

##Congratulations!

For more visit my blog: https://www.patrykgalach.com
