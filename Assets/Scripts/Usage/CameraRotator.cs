﻿using UnityEngine;

/// <summary>
/// Camera Rotator.
/// This is example how you can chain few animations together
/// </summary>
public class CameraRotator : MonoBehaviour
{
    /// <summary>
    /// Unity method called on first frame.
    /// This start animation chain.
    /// </summary>
    private void Start()
    {
        AddWaitAnimation();
    }

    /// <summary>
    /// Method that add rotate animation to camera root.
    /// </summary>
    private void AddRotateAnimation()
    {
        // Attaching animation component.
        var rotateAnim = gameObject.AddComponent<RotateByAnimation>();
        // Set what curve to use to animate rotation.
        rotateAnim.animationCurve = BaseAnimation.AnimationCurveEnum.Hermite;
        // Set wait duration.
        rotateAnim.duration = 1;
        // Set rotation offset.
        rotateAnim.targetOffset = Vector3.up * 90;

        // Assign what should happen after animation is finished.
        // Add wait in this case.
        rotateAnim.OnAnimationFinished += AddWaitAnimation;

        // Start wait animation.
        rotateAnim.StartAnimation();
    }

    /// <summary>
    /// Method that add wait animation to camera root.
    /// </summary>
    private void AddWaitAnimation()
    {
        // Attaching animation component.
        var waitAnim = gameObject.AddComponent<WaitAnimation>();
        // Set wait duration.
        waitAnim.duration = 3;

        // Assign what should happen after animation is finished.
        // Add rotation in this case.
        waitAnim.OnAnimationFinished += AddRotateAnimation;

        // Start wait animation.
        waitAnim.StartAnimation();
    }
}
