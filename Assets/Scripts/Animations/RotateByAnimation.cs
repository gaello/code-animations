﻿using UnityEngine;

/// <summary>
/// Rotate By animation.
/// Component should get by how much it should rotate.
/// </summary>
public class RotateByAnimation : BaseAnimation
{
    [Header("Rotate By")]
    // How much it should rotate.
    public Vector3 targetOffset;

    // Starting rotation.
    private Vector3 startRotation;
    // Calculated end rotation.
    private Vector3 targetRotation;

    /// <summary>
    /// Initializing variables for animation.
    /// </summary>
    public override void StartAnimation()
    {
        startRotation = transform.localEulerAngles;
        targetRotation = startRotation + targetOffset;
        base.StartAnimation();
    }

    /// <summary>
    /// Internal animation loop.
    /// </summary>
    /// <param name="t">t receives values from 0.0 to 1.0.</param>
    protected override void UpdateAnimation(float t)
    {
        base.UpdateAnimation(t);
        transform.localEulerAngles = CurvedValue(startRotation, targetRotation, t);
    }

    /// <summary>
    /// On finish rotate object to target rotation.
    /// </summary>
    protected override void FinishAnimation()
    {
        transform.localEulerAngles = targetRotation;
        base.FinishAnimation();
    }
}
