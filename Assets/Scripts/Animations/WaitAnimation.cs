﻿using UnityEngine;

/// <summary>
/// Wait animation.
/// This "animation" is used only for waiting purpose.
/// </summary>
public class WaitAnimation : BaseAnimation
{

}
