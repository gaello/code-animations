﻿using UnityEngine;

/// <summary>
/// Move Hover animation.
/// Component will move by target offset and then back to start position.
/// </summary>
public class MoveHoverAnimation : BaseAnimation
{
    [Header("Move Hover")]
    // Hover by how much
    public Vector3 targetOffset;

    // Starting position.
    private Vector3 startPosition;
    // Calculated end position.
    private Vector3 targetPosition;

    /// <summary>
    /// Initializing variables for animation.
    /// </summary>
    public override void StartAnimation()
    {
        startPosition = transform.localPosition;
        targetPosition = startPosition + targetOffset;
        base.StartAnimation();
    }

    /// <summary>
    /// Internal animation loop.
    /// </summary>
    /// <param name="t">t receives values from 0.0 to 1.0.</param>
    protected override void UpdateAnimation(float t)
    {
        base.UpdateAnimation(t);
        transform.localPosition = CurvedValue(startPosition, targetPosition, Mathf.PingPong(t, 0.5f) * 2);
    }

    /// <summary>
    /// On finish move object to end position.
    /// </summary>
    protected override void FinishAnimation()
    {
        transform.localPosition = startPosition;
        base.FinishAnimation();
    }
}
