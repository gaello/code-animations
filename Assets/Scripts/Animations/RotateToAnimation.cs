﻿using UnityEngine;

/// <summary>
/// Rotate To animation.
/// Component should get how it should rotate.
/// </summary>
public class RotateToAnimation : BaseAnimation
{
    [Header("Rotate To")]
    // How it should rotate.
    public Vector3 targetRotation;

    // Starting rotation.
    private Vector3 startRotation;

    /// <summary>
    /// Initializing variables for animation.
    /// </summary>
    public override void StartAnimation()
    {
        startRotation = transform.localEulerAngles;
        base.StartAnimation();
    }

    /// <summary>
    /// Internal animation loop.
    /// </summary>
    /// <param name="t">t receives values from 0.0 to 1.0.</param>
    protected override void UpdateAnimation(float t)
    {
        base.UpdateAnimation(t);
        transform.localEulerAngles = CurvedValue(startRotation, targetRotation, t);
    }

    /// <summary>
    /// On finish rotate object to target rotation.
    /// </summary>
    protected override void FinishAnimation()
    {
        transform.localEulerAngles = targetRotation;
        base.FinishAnimation();
    }
}
